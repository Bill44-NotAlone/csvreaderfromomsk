package prof.code.model;

import lombok.Data;

import java.net.URL;
import java.time.LocalDate;

@Data
public class Decree {
    private String codeOfDecree;
    private String orderNumber;
    private Contractor contractor;
    private Customer customer;
    private Address address;
    private String typeOfRestriction;
    private String typeOfWork;
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate liftedDate;
    private LocalDate clarificationDeadline;
    private String documents;
    private String monitoring;
    private String grounds;
    private String psp;
    private String msk;
    private String administrativeCase;
}
