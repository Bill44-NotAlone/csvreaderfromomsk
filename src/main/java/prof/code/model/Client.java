package prof.code.model;

import lombok.Data;

@Data
public class Client {
    private String title;
    private String taxpayerIdentificationNumber;
}
