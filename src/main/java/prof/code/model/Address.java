package prof.code.model;

import lombok.Data;

@Data
public class Address {
    private String address;
    private District district;
}
