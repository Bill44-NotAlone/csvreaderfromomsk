package prof.code.model;

import lombok.Data;

@Data
public class District {
    private String title;

    public District(String title){
        this.title = title;
    }
}
