package prof.code.model;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import lombok.Getter;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ParserProvider {

    private String[] headers;
    private List<District> districts;
    @Getter
    private List<Customer> customers;
    @Getter
    private List<Contractor> contractors;
    @Getter
    private List<Decree> decrees;

    public ParserProvider(String scvFilePath) throws IOException, CsvException {
        districts = new ArrayList<>();
        customers = new ArrayList<>();
        contractors = new ArrayList<>();
        decrees = new ArrayList<>();

        Reader reader = Files.newBufferedReader(Paths.get(scvFilePath));
        CSVReader csvReader = new CSVReader(reader);
        headers = csvReader.readNext();
        csvReader.readAll().forEach(row -> {
            Customer customer = null;
            Contractor contractor = null;
            District district = null;
            Decree decree = new Decree();
            for (int i = 0; i < headers.length; i++){
                String h = headers[i];
                String item = row[i];
                switch (h) {
                    case "Код":
                        decree.setCodeOfDecree(item);
                        break;
                    case "Номер распоряжения":
                        decree.setOrderNumber(item);
                        break;
                    case "Заказчик":
                        customer = customers.stream().filter(c -> c.getTitle().contentEquals(item)).findFirst().orElse(null);
                        if (customer == null) {
                            customer = new Customer();
                            customer.setTitle(item);
                            customers.add(customer);
                        }
                        decree.setCustomer(customer);
                        break;
                    case "ИНН Заказчика":
                        decree.getCustomer().setTaxpayerIdentificationNumber(item);
                        break;
                    case "Подрядчик":
                        contractor = contractors.stream().filter(c -> c.getTitle().contentEquals(item)).findFirst().orElse(null);
                        if (contractor == null) {
                            contractor = new Contractor();
                            contractor.setTitle(item);
                            contractors.add(contractor);
                        }
                        decree.setContractor(contractor);
                        break;
                    case "ИНН Подрядчика":
                        decree.getContractor().setTaxpayerIdentificationNumber(item);
                        break;
                    case "Район":
                        district = districts.stream().filter(d -> d.getTitle().contentEquals(item)).findFirst().orElse(null);
                        if (district == null) {
                            district = new District(item);
                            districts.add(district);
                        }
                        decree.setAddress(new Address());
                        decree.getAddress().setDistrict(district);
                    case "Адрес":
                        decree.getAddress().setAddress(item);
                        break;
                    case "Вид ограничения":
                        decree.setTypeOfRestriction(item);
                        break;
                    case "Вид работ":
                        decree.setTypeOfWork(item);
                        break;
                    case "Дата начала ограничения":
                        decree.setStartDate(Pattern.matches("^\\d{8}$", item) ? LocalDate.parse(item, DateTimeFormatter.BASIC_ISO_DATE) : null);
                        break;
                    case "Дата окончания ограничения":
                        decree.setEndDate(Pattern.matches("^\\d{8}$", item) ? LocalDate.parse(item, DateTimeFormatter.BASIC_ISO_DATE) : null);
                        break;
                    case "Дата снятия ограничения":
                        decree.setLiftedDate(Pattern.matches("^\\d{8}$", item) ? LocalDate.parse(item, DateTimeFormatter.BASIC_ISO_DATE) : null);
                        break;
                    case "Уточнение срока":
                        decree.setClarificationDeadline(Pattern.matches("^\\d{8}$", item) ? LocalDate.parse(item, DateTimeFormatter.BASIC_ISO_DATE) : null);
                        break;
                    case "Документы":
                        decree.setDocuments(item);
                        break;
                    case "Мониторинг":
                        decree.setMonitoring(item);
                        break;
                    case "Основание":
                        decree.setGrounds(Pattern.matches("^[hH]ttp", item) ? item : null);
                        break;
                    case "ПСП":
                        decree.setPsp(item);
                        break;
                    case "МСК":
                        decree.setMsk(item);
                        break;
                    case "Административное дело":
                        decree.setAdministrativeCase(item);
                        break;
                }
            }
            decrees.add(decree);
        });
    }

    public List<Decree> findAll(LocalDate date) {
        return decrees.stream().filter(d -> d.getStartDate().isEqual(date)).collect(Collectors.toList());
    }
}
