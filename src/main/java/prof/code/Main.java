package prof.code;

import com.opencsv.exceptions.CsvException;
import prof.code.model.Decree;
import prof.code.model.ParserProvider;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException, CsvException {
        System.out.println("Введите полный путь CSV файла");
        Scanner scanner = new Scanner(System.in);
        ParserProvider parserProvider = new ParserProvider(scanner.nextLine());
        System.out.printf("%s \n%s \n%s \n", "1. Водите дату в виде dd-mm-yyyy", "2. если вы хотите сменить файл введите change <путь к файлу>", "Если хотите выйти из приложения введите 1.");
        while (true) {
            String input = scanner.nextLine();
            if (Pattern.matches("^1$", input))
                break;
            if (Pattern.matches("^change [A-Za-zА-Яа-яёЁ]+.csv$", input)) {
                input = input.split(" ")[1];
                parserProvider = new ParserProvider(input);
                continue;
            }
            if (Pattern.matches("\\d\\d-\\d\\d-\\d\\d\\d\\d", input)) {
                LocalDate date = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                List<Decree> decrees = parserProvider.findAll(date);
                decrees.stream().forEach(d -> System.out.println(d.getOrderNumber()));
                System.out.println(decrees.stream().count());
                continue;
            }
        }
    }
}